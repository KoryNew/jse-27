package ru.tsk.vkorenygin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractCommand;
import com.jcabi.manifests.Manifests;

public class AboutCommand extends AbstractCommand {

    @Override
    public @Nullable String arg() {
        return "-a";
    }

    @Override
    public @NotNull String name() {
        return "about";
    }

    @Override
    public @Nullable String description() {
        return "display developer info";
    }

    @Override
    public void execute() {
        System.out.println("- ABOUT -");
        @NotNull final String developer = Manifests.read("developer");
        System.out.println("Developer: " + developer);
        @NotNull final String email = Manifests.read("email");
        System.out.println("E-mail: " + email);
    }

}
