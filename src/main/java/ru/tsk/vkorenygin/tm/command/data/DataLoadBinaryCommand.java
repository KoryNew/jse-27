package ru.tsk.vkorenygin.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.command.AbstractDataCommand;
import ru.tsk.vkorenygin.tm.dto.Domain;
import ru.tsk.vkorenygin.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public final class DataLoadBinaryCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public  String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-load-binary";
    }

    @NotNull
    @Override
    public String description() {
        return "Load binary data from file";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[LOAD BINARY DATA]");
        @NotNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }


}
