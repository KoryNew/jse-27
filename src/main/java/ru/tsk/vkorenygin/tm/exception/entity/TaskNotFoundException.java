package ru.tsk.vkorenygin.tm.exception.entity;

import ru.tsk.vkorenygin.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error! Task not found.");
    }

}
