package ru.tsk.vkorenygin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsk.vkorenygin.tm.api.repository.ITaskRepository;
import ru.tsk.vkorenygin.tm.api.service.ITaskService;
import ru.tsk.vkorenygin.tm.entity.Task;
import ru.tsk.vkorenygin.tm.enumerated.Status;
import ru.tsk.vkorenygin.tm.exception.AbstractException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyDescriptionException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyIdException;
import ru.tsk.vkorenygin.tm.exception.empty.EmptyNameException;
import ru.tsk.vkorenygin.tm.exception.entity.TaskNotFoundException;
import ru.tsk.vkorenygin.tm.exception.enumerated.IncorrectStatusException;
import ru.tsk.vkorenygin.tm.exception.system.IncorrectIndexException;
import ru.tsk.vkorenygin.tm.exception.user.AccessDeniedException;
import ru.tsk.vkorenygin.tm.util.DataUtil;

import java.util.Optional;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        task.setUserId(userId);
        taskRepository.add(task);
    }

    @Override
    public void create(final @Nullable String name, final @Nullable String description, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
    }

    @Override
    public @NotNull Optional<Task> findByIndex(final int index,
                                               final @NotNull String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize()) throw new TaskNotFoundException();
        return taskRepository.findByIndex(index, userId);
    }

    @Override
    public @NotNull Optional<Task> findByName(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findByName(name, userId);
    }

    @Override
    public @NotNull Task changeStatusById(final @Nullable String id, final @Nullable Status status, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new IncorrectStatusException();
        return taskRepository.changeStatusById(id, status, userId);
    }

    @Override
    public @NotNull Task changeStatusByName(final @Nullable String name,
                                            final @Nullable Status status,
                                            final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new IncorrectStatusException();
        return taskRepository.changeStatusByName(name, status, userId);
    }

    @Override
    public @NotNull Task changeStatusByIndex(final @Nullable Integer index,
                                             final @Nullable Status status,
                                             final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize()) throw new TaskNotFoundException();
        if (status == null) throw new IncorrectStatusException();
        return taskRepository.changeStatusByIndex(index, status, userId);
    }

    @Override
    public @NotNull Task startById(final @Nullable String id, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.startById(id, userId);
    }

    @Override
    public @NotNull Task startByIndex(final @Nullable Integer index, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize()) throw new TaskNotFoundException();
        return taskRepository.startByIndex(index, userId);
    }

    @Override
    public @NotNull Task startByName(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.startByName(name, userId);
    }

    @Override
    public @NotNull Task finishById(final @Nullable String id, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.finishById(id, userId);
    }

    @Override
    public @NotNull Task finishByIndex(final @Nullable Integer index, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize()) throw new TaskNotFoundException();
        return taskRepository.finishByIndex(index, userId);
    }

    @Override
    public @NotNull Task finishByName(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.finishByName(name, userId);
    }

    @Override
    public @NotNull Task updateByIndex(@Nullable Integer index,
                                       @Nullable String name,
                                       @Nullable String description,
                                       @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize()) throw new TaskNotFoundException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();

        @NotNull final Optional<Task> task = findByIndex(index, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setName(name));
        task.ifPresent(e -> e.setName(description));
        return task.get();
    }

    @Override
    public @NotNull Task updateById(@Nullable String id,
                                    @Nullable String name,
                                    @Nullable String description,
                                    @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();

        @NotNull final Optional<Task> task = findById(id, userId);
        if (!task.isPresent()) throw new TaskNotFoundException();
        task.ifPresent(e -> e.setName(name));
        task.ifPresent(e -> e.setName(description));
        return task.get();
    }

    @Override
    public @NotNull Optional<Task> removeByIndex(final int index,
                                                 final @NotNull String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IncorrectIndexException();
        if (index - 1 > taskRepository.getSize()) throw new TaskNotFoundException();
        return taskRepository.removeByIndex(index, userId);
    }

    @Override
    public @NotNull Optional<Task> removeByName(final @Nullable String name, final @Nullable String userId) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.removeByName(name, userId);
    }

}
